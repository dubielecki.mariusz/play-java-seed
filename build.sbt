name := "play-java-seed"

organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.10.6"

// Configure Java compiler appropriately.
javacOptions ++= Seq(
  "-source", "1.8",
  "-target", "1.8"
)
